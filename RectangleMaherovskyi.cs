﻿using System;
using System.Text;

namespace Epam.ITLab.TestPackage1.Rectangle.Maherovskyi
{
    /// <summary>
    /// ENUM of 4 straight ways.
    /// For Rectangle.Move() method.
    /// </summary>
    public enum Way
    {
        Left,
        Right,
        Up,
        Down
    }

    /// <summary>
    /// Realizes 2-dimentional coordinate points.
    /// </summary>
    class Coordinate
    {
        /// <summary>
        /// Class constuctor.
        /// </summary>
        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Coordinates.
        /// </summary>
        public int X { get; set; }
        public int Y { get; set; }

        /// <summary>
        /// Returns the coordinate with smallest x and y of two.
        /// </summary>
        public static Coordinate SmallestOfTwo(Coordinate a, Coordinate b)
        {
            int smallestX;
            int smallestY;

            if (a.X < b.X)
            {
                smallestX = a.X;
            }
            else
            {
                smallestX = b.X;
            }

            if (a.Y < b.Y)
            {
                smallestY = a.Y;
            }
            else
            {
                smallestY = b.Y;
            }

            return new Coordinate(smallestX, smallestY);
        }

        /// <summary>
        /// Returns the coordinate with biggest x and y of two.
        /// </summary>
        public static Coordinate BiggestOfTwo(Coordinate a, Coordinate b)
        {
            int biggestX;
            int biggestY;

            if (a.X > b.X)
            {
                biggestX = a.X;
            }
            else
            {
                biggestX = b.X;
            }

            if (a.Y > b.Y)
            {
                biggestY = a.Y;
            }
            else
            {
                biggestY = b.Y;
            }

            return new Coordinate(biggestX, biggestY);
        }

        /// <summary>
        /// Prints coordinate points in a form (x; y).
        /// </summary>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("({0}; {1})", X, Y);
            return sb.ToString();
        }
    }

    /// <summary>
    /// Realizes rectangles overview and operations between them.
    /// </summary>
    class Rectangle
    {
        /// <summary>
        /// Rectangle angle coordinates.
        /// </summary>
        Coordinate _upperLeft;
        Coordinate _downLeft;
        Coordinate _upperRight;
        Coordinate _downRight;

        /// <summary>
        /// Height and width of rectangle.
        /// </summary>
        int Height { get; set; }
        int Width { get; set; }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="upperLeft"> Upper left rectangle angle coordinates. </param>
        /// <param name="downRight"> Down right rectangle angle coordinates. </param>
        public Rectangle(Coordinate upperLeft, Coordinate downRight)
        {
            _upperLeft = upperLeft;
            _downRight = downRight;
            _downLeft = new Coordinate(upperLeft.X, downRight.Y);
            _upperRight = new Coordinate(downRight.X, upperLeft.Y);

            Height = _upperLeft.Y - _downLeft.Y;
            Width = _upperRight.X - _upperLeft.X;

            if (Height <= 0 || Width <= 0)
            {
                throw new System.ArgumentException("Wrong coordinates entered");
            }
        }

        /// <summary>
        /// Constructor for crossing method.
        /// </summary>
        Rectangle() { }

        //public Rectangle Cross(Rectangle a, Rectangle b)
        //{
        //    Coordinate a;
        //    Coordinate b;
        //    Coordinate c;
        //    Coordinate d;
        //
        //    for (int i = a.X)
        //}

        /// <summary>
        /// Move the rectangle into the selected way.
        /// </summary>
        /// <param name="w"> Way there to move rectabge. Taken from Way ENUM. </param>
        /// <param name="range"> How far move the rectangle. </param>
        public void Move(Way w, int range)
        {
            switch (w)
            {
                case Way.Down:
                    {
                        _upperLeft.Y -= range;
                        _upperRight.Y -= range;
                        _downLeft.Y -= range;
                        _downRight.Y -= range;
                        break;
                    }
                case Way.Up:
                    {
                        _upperLeft.Y += range;
                        _upperRight.Y += range;
                        _downLeft.Y += range;
                        _downRight.Y += range;
                        break;
                    }
                case Way.Left:
                    {
                        _upperLeft.X -= range;
                        _upperRight.X -= range;
                        _downLeft.X -= range;
                        _downRight.X -= range;
                        break;
                    }
                case Way.Right:
                    {
                        _upperLeft.X += range;
                        _upperRight.X += range;
                        _downLeft.X += range;
                        _downRight.X += range;
                        break;
                    }
            }
        }

        /// <summary>
        /// Increases the size of rectange. Moving coordinates.
        /// </summary>
        /// <param name="x"> How much increase width. </param>
        /// <param name="y"> How much increase heigth. </param>
        public void Increase(int x, int y)
        {
            Height += y;
            Width += x;

            _upperLeft.Y += y;
            _upperRight.Y += y;

            _upperRight.X += x;
            _downRight.X += x;
        }

        /// <summary>
        /// Decreases the size of rectange. Moving coordinates.
        /// </summary>
        /// <param name="x"> How much decrease width. </param>
        /// <param name="y"> How much decrease heigth. </param>
        public void Decrease(int x, int y)
        {
            // Check if not decreasing too much.
            if (Height <= y || Width <= x)
            {
                throw new ArgumentOutOfRangeException("You cannot decrease the size of rectangle more than it's width or height", "original");
            }

            Height -= y;
            Width -= x;

            _upperLeft.Y -= y;
            _upperRight.Y -= y;
            _downRight.X -= x;
            _upperRight.X -= x;
        }

        /// <summary>
        /// Creates the smallest rectangle from the crossing of 2 other.
        /// </summary>
        /// <param name="a"> Rectangle 1. </param>
        /// <param name="b"> Rectange 2. </param>
        /// <returns> New rectangle. </returns>
        public static Rectangle SmallestSetOfTwo(Rectangle a, Rectangle b)
        {
            Coordinate smallest = Coordinate.SmallestOfTwo(a._downLeft, b._downLeft);
            Coordinate biggest = Coordinate.BiggestOfTwo(a._upperRight, b._upperRight);

            return new Rectangle(new Coordinate(smallest.X, biggest.Y), new Coordinate(biggest.X, smallest.Y));
        }
        
        /// <summary>
        /// Checks if rectangles cross between themselves.
        /// </summary>
        /// <returns> True if rectangles crossing. </returns>
        public static bool Crossing(Rectangle a, Rectangle b)
        {
            return (a._upperLeft.Y < b._downRight.Y || a._downRight.Y > b._upperLeft.Y || a._downRight.X < b._upperLeft.X || a._upperLeft.X > b._downRight.X);
        }
        

        /// <summary>
        /// Prints rectangle with coordinates.
        /// Example:
        /// (x; y) |ˉˉˉˉˉˉ| (x; y)
        ///        |      |
        ///        |      |
        /// (x; y) |______| (x; y)
        /// </summary>
        /// <returns> String. Graphical introduction of rectangle. </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(512);
            if (Height >= 2 && Width >= 2)
            {
                for (var i = 0; i < Height; i++)
                {
                    for (var j = 0; j < Width; j++)
                    {
                        // Upper & side lanes print.
                        if (i == 0 && j == 0)
                        {
                            sb.AppendFormat("{0}\t|ˉˉ", _upperLeft.ToString());
                        }
                        else if (j == 0 && i != Height - 1)
                        {
                            sb.Append("\t|  ");
                        }
                        else if (j == Width - 1 && i != 0 && i != Height - 1)
                        {
                            sb.Append("  |");
                        }
                        else if (i != 0 && i != Height - 1)
                        {
                            sb.Append("  ");
                        }
                        if (i == 0 && j == Width - 1)
                        {
                            sb.AppendFormat("|   {0}", _upperRight.ToString());
                        }
                        else if (i == 0)
                        {
                            sb.Append("ˉˉ");
                        }

                        // Bottom lane print.
                        if (i == Height - 1 && j == 0)
                        {
                            sb.AppendFormat("{0}\t|__", _downLeft.ToString());
                        }

                        if (i == Height - 1 && j == Width - 1)
                        {
                            sb.AppendFormat("|   {0}", _downRight.ToString());
                        }
                        else if (i == Height - 1)
                        {
                            sb.Append("__");
                        }
                    }
                    sb.Append("\n");
                }
            }
            else
            {
                sb.AppendFormat("a: {0}; b: {1}\n", _upperLeft, _upperRight);
                sb.AppendFormat("c: {0}; d: {1}", _downLeft, _downRight);
            }
            return sb.ToString();
        }  
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Rectangle r = new Rectangle(new Coordinate(-1, 9), new Coordinate(5, 2));
            Rectangle r1 = new Rectangle(new Coordinate(3, 6), new Coordinate(6, 0));
            Console.WriteLine(r);
            Console.WriteLine(r1);
            r.Move(Way.Left, 3);
            r.Increase(3, 0);
            Console.WriteLine(r);
            r.Decrease(0, 3);
            Console.WriteLine(r);
            r = Rectangle.SmallestSetOfTwo(r, r1);
            Console.WriteLine(r);
        }
    }
}